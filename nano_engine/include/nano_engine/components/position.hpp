#pragma once

namespace nano_engine::components
{
	struct Position
	{
		float x;
		float y;
		float z;
	};
}