#pragma once

namespace nano_engine::components
{
	struct Velocity
	{
		float x;
		float y;
		float z;
	};
}